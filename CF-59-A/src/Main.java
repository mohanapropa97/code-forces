import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.nextLine();
        char[] chr = s.toCharArray();
        int lowerCase=0,upperCase=0;
        for(int i=0 ; i<chr.length;i++)
        {
            if (chr[i]>='a' && chr[i]<='z')
                lowerCase++;
            else
                upperCase++;
        }

        if(lowerCase>=upperCase)
            s=s.toLowerCase();
        else
            s=s.toUpperCase();

        System.out.println(s);
    }
}