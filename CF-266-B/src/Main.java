import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n, t;
        n = in.nextInt();
        t = in.nextInt();
        in.nextLine();
        String s = in.nextLine();
        char[] c = s.toCharArray();
        for (int i = 1; i <= t; i++) {
            int j = 0;
            while (j < c.length - 1) {
                if (c[j] == 'B' && c[j + 1] == 'G') {
                    c[j] = 'G';
                    c[j + 1] = 'B';
                    j = j + 2;
                } else
                    j++;
            }
        }
        s = new String(c);
        System.out.println(s);
    }
}