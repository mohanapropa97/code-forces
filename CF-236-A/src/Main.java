import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");
        Scanner in = new Scanner(System.in);
        String name = in.nextLine();

        char[] nameChar = name.toCharArray();
        Arrays.sort(nameChar);
        int count=0;
        for(int i=0;i<nameChar.length-1;i++)
        {
           if(nameChar[i]!=nameChar[i+1])
               count++;
        }
      /*  System.out.println(count);
        for(char i: nameChar)
            System.out.print(i+" ");*/

        if((count+1)%2==0)
            System.out.println("CHAT WITH HER!");
        else
            System.out.println("IGNORE HIM!" );
    }
}